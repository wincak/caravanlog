<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="cs_CZ">
<context>
    <name>AddLogWindow</name>
    <message>
        <location filename="AddLogWindow.qml" line="16"/>
        <source>km</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>AddLogWindowForm.ui</name>
    <message>
        <location filename="AddLogWindowForm.ui.qml" line="14"/>
        <source>New drive log</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="AddLogWindowForm.ui.qml" line="27"/>
        <source>Mileage</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="AddLogWindowForm.ui.qml" line="39"/>
        <source>Comment</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="AddLogWindowForm.ui.qml" line="44"/>
        <source>comment</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>FileIo</name>
    <message>
        <location filename="fileio.cpp" line="19"/>
        <location filename="fileio.cpp" line="75"/>
        <source>File source is not set.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="fileio.cpp" line="23"/>
        <location filename="fileio.cpp" line="79"/>
        <source>File source is not a local path.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="fileio.cpp" line="31"/>
        <location filename="fileio.cpp" line="92"/>
        <source>Failed to open file %1 for writing.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="fileio.cpp" line="45"/>
        <source>Failed to write data.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ReadPageForm.ui</name>
    <message>
        <location filename="ReadPageForm.ui.qml" line="11"/>
        <source>Read</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SubmitPageForm.ui</name>
    <message>
        <location filename="SubmitPageForm.ui.qml" line="15"/>
        <source>Add log</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="SubmitPageForm.ui.qml" line="31"/>
        <source>File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="SubmitPageForm.ui.qml" line="38"/>
        <source>Choose</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <location filename="main.qml" line="15"/>
        <source>CaravanLog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="main.qml" line="31"/>
        <source>Submit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="main.qml" line="34"/>
        <source>Read</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
