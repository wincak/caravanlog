#pragma once

#include <QObject>
#include <QUrl>

class FileIo : public QObject
{
    Q_OBJECT

    Q_PROPERTY(QUrl filePath READ filePath WRITE setFilePath NOTIFY filePathChanged)

    Q_PROPERTY(QString text READ text WRITE setText NOTIFY textChanged)
    Q_PROPERTY(QString error READ error NOTIFY errorChanged)

public:
    explicit FileIo(QObject *parent = nullptr);

    QUrl filePath() const;

    bool appendLine(const QString& line);

    QString text() const;
    QString error() const;

public slots:
    void setFilePath(const QUrl& filePath);

    void setText(const QString& text);

signals:
    void filePathChanged(QUrl filePath);
    void errorChanged(QString error);

    void textChanged(QString text);

private:
    QStringList read();
    void setError(const QString& error);
    QString readContents(QString* error);

private:
    QUrl m_filePath;
    QString m_text;
    QString m_error;
};

