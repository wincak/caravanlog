#include "carlistmodel.h"

CarListModel::CarListModel(QObject *parent)
    : QAbstractListModel(parent)
{
}

QVariant CarListModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    // FIXME: Implement me!
    Q_UNUSED(section)
    Q_UNUSED(orientation)
    Q_UNUSED(role)

    return {};
}

QHash<int, QByteArray> CarListModel::roleNames() const
{
    QHash<int, QByteArray> roleNames = {
        { IdRole, "id" },
        { NameRole, "name" },
        { IconRole, "icon" },
    };

    return roleNames;
}

int CarListModel::rowCount(const QModelIndex &parent) const
{
    // For list models only the root node (an invalid parent) should return the list's size. For all
    // other (valid) parents, rowCount() should return 0 so that it does not become a tree model.
    if (parent.isValid())
        return 0;

    return m_carData.count();
}

QVariant CarListModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();

    int row = index.row();
    if (row >= m_carData.count())
        return {};

    DataLine dataLine = m_carData.at(row);
    QVariant data;
    switch (role) {
    case IdRole:
        data = dataLine.id;
        break;
    case NameRole:
        data = dataLine.name;
        break;
    case IconRole:
        data = dataLine.icon;
        break;
    }

    return data;
}

void CarListModel::setSourceData(QMap<QString, Car *> sourceData)
{
    QList<DataLine> newData;

    for (auto it = sourceData.cbegin(); it != sourceData.cend(); it++) {
        Q_ASSERT(it.value());

        DataLine dataLine;
        dataLine.id = it.key();
        dataLine.name = it.value()->name();
        // TODO: icon

        m_carData.append(dataLine);
    }

    // TODO: indicate data change
    beginResetModel();
    m_carData.swap(newData);
    endResetModel();
}
