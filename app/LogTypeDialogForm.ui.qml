import QtQuick 2.4
import QtQuick.Controls 2.12

Dialog {
    width: 300
    height: 200

    property alias typeCombo: typeCombo

    header: Label {
        id: header
        text: qsTr("Log type")
        font.pixelSize: Qt.application.font.pixelSize * 2
        padding: 10
    }

    ComboBox {
        id: typeCombo
        width: parent.width
    }

    standardButtons: Dialog.Ok | Dialog.Cancel
}
