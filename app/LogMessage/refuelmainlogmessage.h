#pragma once

#include "abstractlogmessage.h"

class RefuelMainLogMessage : public AbstractLogMessage
{
public:
    RefuelMainLogMessage(
        const QDate& date, uint mileage, float volume, float price, const QString& comment);

    // AbstractLogMessage interface
    QDate date() const override;
    QString typeName() const override;
    bool isValid() const override;
    QVariant toVariant() const override;

    QString comment() const;
    uint mileage() const;
    float volume() const;
    float price() const;

    static QString typeCode();
    static RefuelMainLogMessage fromVariant(const QVariant& log);

private:
    QString m_comment;
    uint m_mileage;
    float m_volume;
    float m_price;
};
