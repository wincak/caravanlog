#include "refuelmainlogmessage.h"

#include "utils.h"

const QChar typeCodeChar = 'f';

RefuelMainLogMessage::RefuelMainLogMessage(
    const QDate& date, uint mileage, float volume, float price, const QString& comment)
    : AbstractLogMessage(typeCodeChar, date)
    , m_comment(comment)
    , m_mileage(mileage)
    , m_volume(volume)
    , m_price(price)
{
}

QDate RefuelMainLogMessage::date() const
{
    return m_date;
}

QString RefuelMainLogMessage::comment() const
{
    return m_comment;
}

QString RefuelMainLogMessage::typeName() const
{
    return QObject::tr("Refuelling - main fuel");
}

bool RefuelMainLogMessage::isValid() const
{
    if (!m_date.isValid())
        return false;

    if (m_mileage == 0)
        return false;

    if (m_volume <= 0)
        return false;

    if (m_price <= 0)
        return false;

    // Comment is optional

    return true;
}

QVariant RefuelMainLogMessage::toVariant() const
{
    auto hash = AbstractLogMessage::toVariant().toHash();
    if (hash.isEmpty())
        return hash;

    hash.insert("mileage", m_mileage);
    hash.insert("comment", m_comment);
    hash.insert("volume", m_volume);
    hash.insert("price", m_price);

    // TODO: proper unit implementation
    hash.insert("mileageUnit", "km");
    hash.insert("priceUnit", "czk");

    return QVariant(hash);
}

uint RefuelMainLogMessage::mileage() const
{
    return m_mileage;
}

float RefuelMainLogMessage::volume() const
{
    return m_volume;
}

float RefuelMainLogMessage::price() const
{
    return m_price;
}

QString RefuelMainLogMessage::typeCode()
{
    return typeCodeChar;
}

RefuelMainLogMessage RefuelMainLogMessage::fromVariant(const QVariant &log)
{
    Q_ASSERT(log.type() == QVariant::Hash || log.type() == QVariant::Map);

    auto hash = log.toHash();

    QDate date = hash.value("date").toDate();
    uint mileage = hash.value("mileage").toUInt();
    float volume = hash.value("volume").toFloat();
    float price = hash.value("price").toFloat();
    QString comment = hash.value("comment").toString();

    // TODO units

    auto msg = RefuelMainLogMessage(date, mileage, volume, price, comment);

    return msg;
}
