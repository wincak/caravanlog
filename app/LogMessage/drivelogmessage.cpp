#include "drivelogmessage.h"

#include "utils.h"

const QChar typeCodeChar = 'd';

DriveLogMessage::DriveLogMessage(const QDate &date, uint mileage, const QString &comment)
    : AbstractLogMessage(typeCodeChar, date)
    , m_comment(comment)
    , m_mileage(mileage)
{
}

QString DriveLogMessage::typeName() const
{
    return QObject::tr("Drive");
}

bool DriveLogMessage::isValid() const
{
    if (!m_date.isValid())
        return false;

    if (m_mileage == 0)
        return false;

    // Comment is optional

    return true;
}

QVariant DriveLogMessage::toVariant() const
{
    auto hash = AbstractLogMessage::toVariant().toHash();
    if (hash.isEmpty())
        return hash;

    hash.insert("mileage", m_mileage);
    hash.insert("comment", m_comment);

    // TODO: proper unit implementation
    hash.insert("mileageUnit", "km");

    return QVariant(hash);
}

uint DriveLogMessage::mileage() const
{
    return m_mileage;
}

QString DriveLogMessage::typeCode()
{
    return typeCodeChar;
}

DriveLogMessage DriveLogMessage::fromVariant(const QVariant &log)
{
    Q_ASSERT(log.type() == QVariant::Hash || log.type() == QVariant::Map);

    auto hash = log.toHash();

    QDate date = hash.value("date").toDate();
    uint mileage = hash.value("mileage").toUInt();
    QString comment = hash.value("comment").toString();

    // TODO units

    auto msg = DriveLogMessage(date, mileage, comment);

    return msg;
}

QDate DriveLogMessage::date() const
{
    return m_date;
}

QString DriveLogMessage::comment() const
{
    return m_comment;
}
