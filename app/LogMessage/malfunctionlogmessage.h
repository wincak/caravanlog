#pragma once

#include "abstractlogmessage.h"

class MalfunctionLogMessage : public AbstractLogMessage
{
public:
    MalfunctionLogMessage(const QDate& date, uint mileage, const QString& comment);

    // AbstractLogMessage interface
    QDate date() const override;
    QString typeName() const override;
    bool isValid() const override;

    QString comment() const;
    uint mileage() const;

    QVariantHash toHash() const;

    static QString typeCode();
    static MalfunctionLogMessage fromVariant(const QVariant& log);

private:
    QString m_comment;
    uint m_mileage;
};

