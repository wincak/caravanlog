#include "malfunctionlogmessage.h"

#include "utils.h"

const QChar typeCodeChar = 'e';

MalfunctionLogMessage::MalfunctionLogMessage(const QDate &date, uint mileage, const QString &comment)
    : AbstractLogMessage(typeCodeChar, date)
    , m_comment(comment)
    , m_mileage(mileage)
{
}

QDate MalfunctionLogMessage::date() const
{
    return m_date;
}

QString MalfunctionLogMessage::comment() const
{
    return m_comment;
}

QString MalfunctionLogMessage::typeName() const
{
    return QObject::tr("Malfunction");
}

bool MalfunctionLogMessage::isValid() const
{
    if (!m_date.isValid())
        return false;

    if (m_mileage == 0)
        return false;

    if (m_comment.isEmpty())
        return false;

    return true;
}

uint MalfunctionLogMessage::mileage() const
{
    return m_mileage;
}

QVariantHash MalfunctionLogMessage::toHash() const
{
    auto hash = AbstractLogMessage::toVariant().toHash();
    if (hash.isEmpty())
        return hash;

    hash.insert("mileage", m_mileage);
    hash.insert("comment", m_comment);

    // TODO: proper unit implementation
    hash.insert("mileageUnit", "km");

    return hash;
}

QString MalfunctionLogMessage::typeCode()
{
    return typeCodeChar;
}

MalfunctionLogMessage MalfunctionLogMessage::fromVariant(const QVariant &log)
{
    Q_ASSERT(log.type() == QVariant::Hash || log.type() == QVariant::Map);

    auto hash = log.toHash();

    QDate date = hash.value("date").toDate();
    uint mileage = hash.value("mileage").toUInt();
    QString comment = hash.value("comment").toString();

    // TODO units

    auto msg = MalfunctionLogMessage(date, mileage, comment);

    return msg;
}
