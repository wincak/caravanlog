#pragma once

#include "abstractlogmessage.h"

class DriveLogMessage : public AbstractLogMessage
{
public:
    DriveLogMessage(const QDate& date, uint mileage, const QString& comment);

    // AbstractLogMessage interface
    QDate date() const override;
    QString typeName() const override;
    bool isValid() const override;
    QVariant toVariant() const override;

    QString comment() const;
    uint mileage() const;

    static QString typeCode();
    static DriveLogMessage fromVariant(const QVariant& log);

private:
    QString m_comment;
    uint m_mileage;
};

