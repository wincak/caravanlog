#pragma once

#include <QDate>
#include <QVariantHash>

// TODO
// Consider making both toVariant and fromVariant either static or virtual

class AbstractLogMessage
{
public:
    explicit AbstractLogMessage(const QString& typeCode, const QDate& date);

    QString typeCode() const;

    virtual QDate date() const = 0;
    virtual QString typeName() const = 0;
    virtual bool isValid() const = 0;

    virtual QVariant toVariant() const;

    static QString typeCode(const QVariant& log);

protected:
    QString m_typeCode;
    QDate m_date;
};

