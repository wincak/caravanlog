#include "abstractlogmessage.h"

AbstractLogMessage::AbstractLogMessage(const QString &typeCode, const QDate& date)
    : m_typeCode(typeCode)
    , m_date(date)
{
}

QString AbstractLogMessage::typeCode() const
{
    return m_typeCode;
}

QString AbstractLogMessage::typeCode(const QVariant &log)
{
    Q_ASSERT(log.type() == QVariant::Hash || log.type() == QVariant::Map);

    auto hash = log.toHash();

    auto typeString = hash.value("type").toString();
    Q_ASSERT(!typeString.isEmpty());

    return typeString;
}

QVariant AbstractLogMessage::toVariant() const
{
    if (!isValid())
        return {};

    QVariantHash hash;
    hash.insert("type", m_typeCode);
    hash.insert("date", m_date.toString(Qt::ISODate));

    return QVariant(hash);
}
