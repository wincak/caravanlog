import QtQuick 2.4

LogTypeDialogForm {
    ListModel {
        id: typeModel
        ListElement {
           name: qsTr("Drive")
           prefix: "d"
        }
        ListElement {
            name: qsTr("Refuelling - main fuel")
            prefix: "f"
        }
//        ListElement {
//            name: qsTr("Refuelling - generator")
//            prefix: "g"
//        }
//        ListElement {
//            name: qsTr("Refuelling - propane")
//            prefix: "p"
//        }
        ListElement {
            name: qsTr("Malfunction")
            prefix: "e"
        }
//        ListElement {
//            name: qsTr("Maintenance")
//            prefix: "m"
//        }
//        ListElement {
//            name: qsTr("Repair")
//            prefix: "r"
//        }
//        ListElement {
//            name: qsTr("Other")
//            prefix: "o"
//        }
    }

    typeCombo.textRole: "name"
    typeCombo.valueRole: "prefix"
    typeCombo.model: typeModel
}
