import QtQuick 2.12
import QtQuick.Controls 2.12

import org.kde.kirigami 2.13 as Kirigami

Kirigami.Page {
    property alias carNameEdit: carNameEdit
    property alias carMileageLabel: carMileageLabel
    property alias carFuelUsedLabel: carFuelUsedLabel
    property alias carFuelCostLabel: carFuelCostLabel
    property alias carAvgConsumptionLabel: carAvgConsumptionLabel

    Kirigami.FormLayout {
        id: content
        anchors.fill: parent

        TextField {
            id: carNameEdit
            Kirigami.FormData.label: qsTr("Name")
            enabled: false
        }

        Label {
            id: carMileageLabel
            Kirigami.FormData.label: qsTr("Mileage")
        }
        Label {
            id: carFuelUsedLabel
            Kirigami.FormData.label: qsTr("Total fuel used")
        }
        Label {
            id: carFuelCostLabel
            Kirigami.FormData.label: qsTr("Total fuel cost")
        }
        Label {
            id: carAvgConsumptionLabel
            Kirigami.FormData.label: qsTr("Average consumption");
        }
    }
}
