import QtQuick 2.4

import name.wincak.caravanlog 1.0

CarDetailsPageForm {
    id: page

    title: qsTr("Details (%1)").arg(carName)

    property string carName: ""

    carNameEdit.text: carName

    onCarNameChanged: init()

    function init() {
        const car = Garage.getCar(carName);
        if (!car) {
            // TODO
        }

        carMileageLabel.text = car.mileageString;
        carFuelUsedLabel.text = car.fuelUsedString;
        carFuelCostLabel.text = car.fuelCostString;
        carAvgConsumptionLabel.text = car.avgConsumptionString;
    }
}
