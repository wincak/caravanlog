#pragma once

#include <QObject>
#include <QQmlListProperty>
#include <QUrl>

#include <memory>

#include "car.h"

class Garage : public QObject
{
    Q_OBJECT

    Q_PROPERTY(QUrl dirPath READ dirPath WRITE setDirPath NOTIFY dirPathChanged)

    Q_PROPERTY(QList<QObject*> cars READ cars NOTIFY carsChanged)

public:
    explicit Garage(QObject *parent = nullptr);

    Q_INVOKABLE Car* getCar(const QString& name);
    Q_INVOKABLE bool createCar(const QString& name);
    Q_INVOKABLE bool deleteCar(const QString& name);

    QUrl dirPath() const;

    QList<QObject*> cars() const;

public slots:
    void setDirPath(QUrl dirPath);

signals:
    void dirPathChanged(QUrl dirPath);

    void carsChanged(QList<QObject*> cars);

private:
    void indexDirectory();

private:
    QUrl m_dirPath;
    QList<Car*> m_cars;
};
