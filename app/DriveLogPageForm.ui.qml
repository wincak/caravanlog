import QtQuick 2.12
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.12

import org.kde.kirigami 2.13 as Kirigami

Kirigami.Page {
    title: qsTr("Add drive log")

    property alias saveAction: saveAction
    property alias dateSetButton: dateSetButton
    property alias datePicker: datePicker
    property alias datePickerSheet: datePickerSheet
    property alias datePickerOkButton: datePickerOkButton
    property alias datePickerCancelButton: datePickerCancelButton
    property alias mileage: mileage
    property alias comment: comment
    property alias carNotFoundMessage: carNotFoundMessage
    property alias failedToCreateLogMessage: failedToCreateLogMessage
    property alias selectedDateLabel: selectedDateLabel

    signal saveActionTriggered

    actions.main: Kirigami.Action {
        id: saveAction
        text: qsTr("Save")
    }

    // Date picker
    Kirigami.OverlaySheet {
        id: datePickerSheet
        header: Kirigami.Heading {
            text: qsTr("Date")
        }
        footer: RowLayout {
            Label {
                text: qsTr("Footer:")
            }
            Button {
                id: datePickerOkButton
                text: qsTr("OK")
            }
            Button {
                id: datePickerCancelButton
                text: qsTr("Cancel")
            }
        }
        DatePicker {
            id: datePicker
        }
    }

    // Edit form
    Kirigami.FormLayout {
        id: content
        anchors.fill: parent

        TextField {
            id: selectedDateLabel
            Kirigami.FormData.label: qsTr("Date")
            text: qsTr("Today")
            enabled: false
        }
        Button {
            id: dateSetButton
            text: qsTr("Set")
        }
        SpinBox {
            id: mileage
            Kirigami.FormData.label: qsTr("Mileage")
            wheelEnabled: true
            editable: true
            to: 9999999
        }
        TextArea {
            id: comment
            Kirigami.FormData.label: qsTr("Comment")
            placeholderText: qsTr("comment")
            implicitWidth: 200
            implicitHeight: 100
        }

        // Error messages
        Kirigami.InlineMessage {
            id: carNotFoundMessage
            Layout.fillWidth: true
            type: Kirigami.MessageType.Error
            text: qsTr("Car not found")
        }
        Kirigami.InlineMessage {
            id: failedToCreateLogMessage
            Layout.fillWidth: true
            type: Kirigami.MessageType.Error
            text: qsTr("Failed to create log")
        }
    }
}

/*##^##
Designer {
    D{i:0;autoSize:true;height:480;width:640}
}
##^##*/
