import QtQuick 2.4
import QtQuick.Controls 2.12

import org.kde.kirigami 2.13 as Kirigami

Kirigami.Page {
    title: qsTr("Settings")

    property alias username: username
    property alias chooseDataDirectoryButton: chooseDataDirectoryButton
    property alias dataDirectoryPath: dataDirectoryPath

    Kirigami.FormLayout {
        anchors.fill: parent

        TextField {
            id: dataDirectoryPath
            Kirigami.FormData.label: qsTr("Data directory path")
            readOnly: true
            placeholderText: qsTr("path")
        }
        Button {
            id: chooseDataDirectoryButton
            text: qsTr("Choose")
        }
        TextField {
            id: username
            Kirigami.FormData.label: qsTr("Username")
            placeholderText: qsTr("Short user identificator (optional)")
        }
    }
}
