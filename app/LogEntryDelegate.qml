import QtQuick 2.4

import org.kde.kirigami 2.13 as Kirigami

LogEntryDelegateForm {
    property string logDateText: qsTr("<date>")
    property string logTypeNameText: qsTr("<type>")
    property string logTypeCode: "x"
    property string logDescriptionText: qsTr("<description>")
    property string mileageText: qsTr("<mileage>")

    dateTimeLabel.text: logDateText
    logTypeLabel.text: logTypeNameText
    descriptionLabel.text: logDescriptionText
    mileageLabel.text: mileageText

    states: [
        State {
            name: "Repair"
            when: logTypeCode === "r"
            PropertyChanges {
                target: itemIcon
                text: "🔧"
            }
        },
        State {
            name: "Drive"
            when: logTypeCode === "d"
            PropertyChanges {
                target: itemIcon
                text: "➡"
            }
        },
        State {
            name: "Refuelling"
            when: logTypeCode === "f" || logTypeCode === "g" || logTypeCode === "p"
            PropertyChanges {
                target: itemIcon
                text: "⛽"
            }
        },
        State {
            name: "Malfunction"
            when: logTypeCode === "e"
            PropertyChanges {
                target: itemIcon
                text: "🔥"
            }
        }
    ]
}
