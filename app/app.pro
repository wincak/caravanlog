QT += widgets quick
android: {
    QT += androidextras
}

SOURCES += \
    LogMessage/abstractlogmessage.cpp \
    LogMessage/drivelogmessage.cpp \
    LogMessage/malfunctionlogmessage.cpp \
    LogMessage/refuelmainlogmessage.cpp \
    car.cpp \
    carlistmodel.cpp \
    fileio.cpp \
    garage.cpp \
    loglist.cpp \
    main.cpp \
    utils.cpp

DISTFILES += \
    *.qml \
    AddMalfunctionLogDialogForm.ui.qml \
    AddMalfunctionLogPage.qml \
    AddRefuellingLogPage.qml \
    AddRefuellingLogPageForm.ui.qml \
    CarDetailsPage.qml \
    CarDetailsPageForm.ui.qml \
    CarsPage.qml \
    CarsPageForm.ui.qml \
    CreateCarDialog.qml \
    CreateCarDialogForm.ui.qml \
    DatePicker.qml \
    DriveLogPageForm.ui.qml \
    LogTypeDialog.qml \
    LogTypeDialogForm.ui.qml \
    SettingsDialog.qml \
    SettingsDialogForm.ui.qml \
    SettingsPage.qml \
    SettingsPageForm.ui.qml \
    TodoPage.qml

HEADERS += \
    LogMessage/abstractlogmessage.h \
    LogMessage/drivelogmessage.h \
    LogMessage/malfunctionlogmessage.h \
    LogMessage/refuelmainlogmessage.h \
    car.h \
    carlistmodel.h \
    fileio.h \
    garage.h \
    loglist.h \
    utils.h

RESOURCES += qml.qrc
