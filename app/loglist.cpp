#include "loglist.h"

#include "LogMessage/drivelogmessage.h"
#include "LogMessage/malfunctionlogmessage.h"
#include "LogMessage/refuelmainlogmessage.h"

LogList::LogList(QObject *parent)
    : QAbstractListModel(parent)
{
}

QVariant LogList::headerData(int section, Qt::Orientation orientation, int role) const
{
    // FIXME: Implement me!
    Q_UNUSED(section)
    Q_UNUSED(orientation)
    Q_UNUSED(role)

    return {};
}

QHash<int, QByteArray> LogList::roleNames() const
{
    QHash<int, QByteArray> roleNames = {
        { DateRole, "date" },
        { TypeNameRole, "typeName" },
        { TypeCodeRole, "typeCode" },
        { MileageRole, "mileage" },
        { CommentRole, "comment" },
        { DescriptionRole, "description" },
    };

    return roleNames;
}

int LogList::rowCount(const QModelIndex &parent) const
{
    // For list models only the root node (an invalid parent) should return the list's size. For all
    // other (valid) parents, rowCount() should return 0 so that it does not become a tree model.
    if (parent.isValid())
        return 0;

    return m_parsedData.count();
}

QVariant LogList::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return {};

    int row = index.row();
    if (row >= m_parsedData.count())
        return {};

    DataLine dataLine = m_parsedData.at(row);
    QVariant data;
    switch (role) {
    case DateRole:
        data = dataLine.date;
        break;
    case TypeNameRole:
        data = dataLine.typeName;
        break;
    case TypeCodeRole:
        data = dataLine.typeCode;
        break;
    case MileageRole:
        data = dataLine.mileage;
        break;
    case CommentRole:
        data = dataLine.comment;
        break;
    case DescriptionRole:
        data = dataLine.description;
        break;
    }

    return data;
}

void LogList::setSourceData(QList<AbstractLogMessage *> logMessages)
{
    QVector<DataLine> newData;
    newData.reserve(logMessages.count());

    QLocale locale = QLocale::system();

    for (auto log : logMessages) {
        Q_ASSERT(log->isValid());

        DataLine dataLine;

        if (log->typeCode() == DriveLogMessage::typeCode()) {
            auto msg = dynamic_cast<DriveLogMessage*>(log);
            Q_ASSERT(msg);

            dataLine.date = locale.toString(msg->date());
            dataLine.typeName = msg->typeName();
            dataLine.typeCode = msg->typeCode();
            dataLine.mileage = QString("%1 km").arg(msg->mileage());
            dataLine.comment = msg->comment();

            dataLine.description = msg->comment();

            newData.append(dataLine);
        } else if (log->typeCode() == MalfunctionLogMessage::typeCode()) {
            auto msg = dynamic_cast<MalfunctionLogMessage*>(log);
            Q_ASSERT(msg);

            dataLine.date = locale.toString(msg->date());
            dataLine.typeName = msg->typeName();
            dataLine.typeCode = msg->typeCode();
            dataLine.mileage = QString("%1 km").arg(msg->mileage());
            dataLine.comment = msg->comment();

            dataLine.description = msg->comment();

            newData.append(dataLine);
        } else if (log->typeCode() == RefuelMainLogMessage::typeCode()) {
            auto msg = dynamic_cast<RefuelMainLogMessage*>(log);
            Q_ASSERT(msg);

            dataLine.date = locale.toString(msg->date());
            dataLine.typeName = msg->typeName();
            dataLine.typeCode = msg->typeCode();
            dataLine.mileage = QString("%1 km").arg(msg->mileage());
            dataLine.comment = msg->comment();

            // TODO: support other units
            dataLine.description = QString("%1 l - %2 CZK").arg(QString::number(msg->volume()),
                                                                QString::number(msg->price()));

            newData.append(dataLine);
        } else {
            qWarning("Unknown log message type code %s", qPrintable(log->typeCode()));
        }
    }

    // TODO: indicate data change
    beginResetModel();
    m_parsedData.swap(newData);
    endResetModel();
}
