import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12

import org.kde.kirigami 2.13 as Kirigami
import name.wincak.caravanlog 1.0

Kirigami.Page {
    title: qsTr("TODO (%1)").arg(carName)

    property string carName: ""

    onCarNameChanged: {
        const car = Garage.getCar(carName);
        fileIo.filePath = car.todoFilePath;
    }

    FileIo {
        id: fileIo
    }

    ColumnLayout {
        anchors.fill: parent

        TextArea {
            id: editor
            text: fileIo.text
            Layout.fillWidth: true
            Layout.fillHeight: true
        }
        Kirigami.InlineMessage {
            id: errorMessage
            Layout.fillWidth: true
            type: Kirigami.MessageType.Error
            text: fileIo.error
            visible: fileIo.error
        }
    }

    actions {
        contextualActions: [
            Kirigami.Action {
                text: qsTr("Save")
                onTriggered: fileIo.setText(editor.text)
            }
        ]
    }
}
