import QtQuick 2.12
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.3

Page {
    id: page
    width: 300
    height: 500

    property alias addLogButton: addLogButton
    property alias fileUrlView: fileUrlView
    property alias chooseFileButton: chooseFileButton
    property alias testFsAccessButton: testFsAccessButton

    header: Label {
        text: qsTr("Add log")
        font.pixelSize: Qt.application.font.pixelSize * 2
        padding: 10
    }

    GridLayout {
        id: formLayout
        anchors.fill: parent
        anchors.margins: 10

        columns: 2

        TextField {
            id: fileUrlView
            readOnly: true
            text: qsTr("File")
            placeholderText: ""
            Layout.fillWidth: true
        }

        Button {
            id: chooseFileButton
            text: qsTr("Choose")
        }

        Item {
            id: formSpacer
            Layout.fillHeight: true
        }
    }

    Button {
        id: addLogButton
        text: "New log"
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 10
        anchors.right: parent.right
        anchors.rightMargin: 10
    }

    Button {
        id: testFsAccessButton
        text: "Test FS access"
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 10
        anchors.left: parent.left
        anchors.leftMargin: 10
    }
}
