#pragma once

#include "car.h"

#include <QAbstractListModel>
#include <QImage>

class CarListModel : public QAbstractListModel
{
    Q_OBJECT

public:
    enum CarRole {
        IdRole,
        NameRole,
        IconRole,
    };

    explicit CarListModel(QObject *parent = nullptr);

    // Header:
    QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;

    // Roles:
    QHash<int, QByteArray> roleNames() const override;

    // Basic functionality:
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;

    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

public slots:
    void setSourceData(QMap<QString, Car*> sourceData);

private:
    struct DataLine {
        QString id;
        QString name;
        QImage icon;
    };

private:
    QList<DataLine> m_carData;
};

