import QtQuick 2.12

import name.wincak.caravanlog 1.0

DriveLogPageForm {
    id: driveLogPage

    property string carName: ""
    property date selectedDate: new Date()

    onCarNameChanged: init()

    Connections {
        target: saveAction
        function onTriggered() {
            // Save log
            let car = Garage.getCar(carName);
            if (!car) {
                carNotFoundMessage.visible = true;
                return;
            }

            let success = car.appendDriveLog(selectedDate, mileage.value, comment.text)
            if (!success) {
                failedToCreateLogMessage.visible = true;
                return;
            }

            // Dismiss page
            mainWindow.pageStack.layers.pop();
        }
    }

    Connections {
        target: datePickerOkButton
        function onClicked() {
            // Select date
            selectedDate = datePicker.date;
            selectedDateLabel.text = selectedDate.toDateString();
            datePickerSheet.close();
        }
    }

    Connections {
        target: datePickerCancelButton
        function onClicked() {
            datePickerSheet.close()
        }
    }

    Connections {
        target: dateSetButton
        function onClicked() {
            datePickerSheet.open()
        }
    }

    function init() {
        let car = Garage.getCar(carName);
        mileage.value = car.mileage;
        comment.text = "";
    }
}
