import QtQuick 2.4
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12

import org.kde.kirigami 2.13 as Kirigami

import name.wincak.caravanlog 1.0

AddRefuellingLogPageForm {
    id: addRefuellingLogPageForm

    property string carName: ""
    property date selectedDate: new Date()

    selectedDateLabel.text: selectedDate.toDateString()
    dateSetButton.onClicked: datePicker.open()

    // QML SpinBox does not support fractions natively
    volume.validator: DoubleValidator {
        bottom: Math.min(volume.from, volume.to)
        top: Math.max(volume.from, volume.to)
    }
    volume.textFromValue: function(value, locale) {
        return Number(value / 100).toLocaleString(locale, 'f', volume.decimals)
    }
    volume.valueFromText: function(text, locale) {
        return Number.fromLocaleString(locale, text) * 100
    }

    price.validator: DoubleValidator {
        bottom: Math.min(price.from, price.to)
        top: Math.max(price.from, price.to)
    }
    price.textFromValue: function(value, locale) {
        return Number(value / 100).toLocaleString(locale, 'f', price.decimals)
    }
    price.valueFromText: function(text, locale) {
        return Number.fromLocaleString(locale, text) * 100
    }

    actions.main: Kirigami.Action {
        text: qsTr("Save")
        onTriggered: {
            const success = writeLog();
            if (success)
                mainWindow.pageStack.layers.pop();
        }
    }

    onCarNameChanged: init()

    Kirigami.OverlaySheet {
        id: datePickerSheet
        header: Kirigami.Heading {
            text: qsTr("Date")
        }
        footer: RowLayout {
            Label {
                text: qsTr("Footer:")
            }
            Button {
                text: qsTr("OK")
                onClicked: {
                    driveLogPage.selectDate(datePicker.date);
                    datePickerSheet.close();
                }
            }
            Button {
                text: qsTr("Cancel")
                onClicked: datePickerSheet.close()
            }
        }
        DatePicker {
            id: datePicker
        }
    }

    function init() {
        let car = Garage.getCar(carName);
        if (!car) {
            carNotFoundMessage.visible = true;
            return;
        }

        mileage.value = car.mileage;
        volume.value = 0;
        price.value = 0;
        comment.text = "";
    }

    function writeLog() {
        let car = Garage.getCar(carName);
        if (!car) {
            carNotFoundMessage.visible = true;
            return;
        }

        let success = car.appendRefuelMainLog(selectedDate,
                                              mileage.value,
                                              volume.realValue,
                                              price.realValue,
                                              comment.text)
        if (!success) {
            failedToCreateLogMessage.visible = true;
            return;
        }

        return true;
    }
}
