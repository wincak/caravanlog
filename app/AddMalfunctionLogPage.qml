import QtQuick 2.4

import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12
import Qt.labs.calendar 1.0 as Labs

import org.kde.kirigami 2.13 as Kirigami

import name.wincak.caravanlog 1.0

Kirigami.Page {
    id: addMalfunctionLogPage

    title: qsTr("Add malfunction log")

    property string carName: ""
    property date selectedDate: new Date()

    actions.main: Kirigami.Action {
        text: qsTr("Save")
        onTriggered: {
            const success = writeLog();
            if (success)
                mainWindow.pageStack.layers.pop();
        }
    }

    onCarNameChanged: init()

    function init() {
        let car = Garage.getCar(carName);
        if (!car) {
            carNotFoundMessage.visible = true;
            return;
        }

        mileage.value = car.mileage;
        comment.clear()
        todo.clear()
    }

    function writeLog() {
        let car = Garage.getCar(carName);
        if (!car) {
            carNotFoundMessage.visible = true;
            return;
        }

        let success = car.appendMalfunctionLog(selectedDate, mileage.value, todo.text, comment.text)
        if (!success) {
            failedToCreateLogMessage.visible = true;
            return;
        }

        return true;
    }

    function selectDate(date) {
        addMalfunctionLogPage.selectedDate = date;
        selectedDateLabel.text = selectedDate.toDateString();
    }

    Kirigami.OverlaySheet {
        id: datePickerSheet
        header: Kirigami.Heading {
            text: qsTr("Date")
        }
        footer: RowLayout {
            Label {
                text: qsTr("Footer:")
            }
            Button {
                text: qsTr("OK")
                onClicked: {
                    driveLogPage.selectDate(datePicker.date);
                    datePickerSheet.close();
                }
            }
            Button {
                text: qsTr("Cancel")
                onClicked: datePickerSheet.close()
            }
        }
        DatePicker {
            id: datePicker
        }
    }

    Kirigami.FormLayout {
        id: content
        anchors.fill: parent

        TextField {
            id: selectedDateLabel
            Kirigami.FormData.label: qsTr("Date")
            text: qsTr("Today")
            enabled: false
        }
        Button {
            id: dateSetButton
            text: qsTr("Set")
            onClicked: datePickerSheet.open()
        }

        SpinBox {
            id: mileage
            Kirigami.FormData.label: qsTr("Mileage")
            wheelEnabled: true
            editable: true
            to: 9999999
        }

        TextArea {
            id: comment
            Kirigami.FormData.label: qsTr("Comment")
            placeholderText: qsTr("comment")
            implicitWidth: 200
            implicitHeight: 100
        }
        TextArea {
            id: todo
            Kirigami.FormData.label: qsTr("TODO")
            placeholderText: qsTr("Create TODO")
            Layout.fillWidth: true
            Layout.fillHeight: true
        }

        // Error messages
        Kirigami.InlineMessage {
            id: carNotFoundMessage
            Layout.fillWidth: true
            type: Kirigami.MessageType.Error
            text: qsTr("Car not found")
        }
        Kirigami.InlineMessage {
            id: failedToCreateLogMessage
            Layout.fillWidth: true
            type: Kirigami.MessageType.Error
            text: qsTr("Failed to create log")
        }
    }
}
