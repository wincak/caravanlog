import QtQuick 2.12
import QtQuick.Controls 2.5

import QtQuick.Layouts 1.12
import QtQuick.Dialogs 1.2 as Dlgs

import Qt.labs.settings 1.1

import org.kde.kirigami 2.13 as Kirigami

import name.wincak.caravanlog 1.0

Kirigami.ApplicationWindow {
    id: mainWindow

    visible: true
    width: 400
    height: 650
    title: qsTr("CaravanLog")

    globalDrawer: Kirigami.GlobalDrawer {
        id: drawer

        actions: [
            Kirigami.Action {
                text: qsTr("Cars")

                property bool current: pageStack.currentItem ?
                                           pageStack.currentItem.objectName === "carsPage" : false
                onCurrentChanged: {
                    checked = current;
                }
                onTriggered: {
                    if (current)
                        return;

                    pageStack.clear();
                    pageStack.push(carsPageComponent);
                }
            },
            Kirigami.Action {
                text: qsTr("Settings")

                property bool current: pageStack.currentItem
                                       ? pageStack.currentItem.objectName === "settingsPage" : false
                onCurrentChanged: {
                    checked = current;
                }
                onTriggered: {
                    if (current)
                        return;

                    pageStack.clear();
                    pageStack.push(settingsPageComponent);
                }
            }
        ]
    }

    pageStack.initialPage: carsPageComponent

    Component {
        id: carsPageComponent

        CarsPage {
            id: carsPage
            objectName: "carsPage"
        }
    }
    Component {
        id: settingsPageComponent

        SettingsPage {
        }
    }

    Component {
        id: logListPageComponent

        LogListPage {
        }
    }

    function showCarDetails(carName) {
        mainWindow.pageStack.push(carDetailsPageComponent, { "carName" : carName });
    }

    Component {
        id: carDetailsPageComponent

        CarDetailsPage {
        }
    }

    function showCarLog(carName) {
        mainWindow.pageStack.push(logListPageComponent, { "carName" : carName });
    }

    function createCar() {
        createCarDialogLoader.active = true;
        createCarDialogLoader.item.open();
    }

    Loader {
        id: createCarDialogLoader
        active: false
        anchors.centerIn: parent
        sourceComponent: CreateCarDialog {
            id: createCarDialog
            visible: false
            anchors.centerIn: parent

            onAccepted: Garage.createCar(carName)
            onClosed: {
                createCarDialogLoader.source = ""
                createCarDialogLoader.active = false;
            }
        }
    }

    function addNewLog(carName) {
        logTypeSelector.selectedCarName = carName;
        logTypeSelector.open();
    }

    Kirigami.OverlaySheet {
        id: logTypeSelector

        property string selectedCarName: ""

        header: Label {
            text: qsTr("Select log type")
        }
        contentItem: ColumnLayout {
            Button {
                text: qsTr("Drive")
                Layout.fillWidth: true
                onClicked: {
                    mainWindow.addDriveLog(logTypeSelector.selectedCarName);
                    logTypeSelector.selectedCarName = "";
                    logTypeSelector.close();
                }
            }
            Button {
                text: qsTr("Refuelling")
                Layout.fillWidth: true
                onClicked: {
                    mainWindow.addRefuellingLog(logTypeSelector.selectedCarName);
                    logTypeSelector.selectedCarName = "";
                    logTypeSelector.close();
                }
            }
            Button {
                text: qsTr("Malfunction")
                Layout.fillWidth: true
                onClicked: {
                    mainWindow.addMalfunctionLog(logTypeSelector.selectedCarName);
                    logTypeSelector.selectedCarName = "";
                    logTypeSelector.close();
                }
            }
            Button {
                text: qsTr("Maintenance")
                Layout.fillWidth: true
                enabled: false
            }
            Button {
                text: qsTr("Repair")
                Layout.fillWidth: true
                enabled: false
            }
            Button {
                text: qsTr("Note")
                Layout.fillWidth: true
                enabled: false
            }
        }
    }

    function addDriveLog(carName) {
        mainWindow.pageStack.layers.push(driveLogPageComponent, { "carName" : carName } );
    }
    function addMalfunctionLog(carName) {
        mainWindow.pageStack.layers.push(addMalfunctionLogComponent, { "carName" : carName } );
    }
    function addRefuellingLog(carName) {
        mainWindow.pageStack.layers.push(addRefuellingLogComponent, { "carName" : carName } );
    }

    Component {
        id: driveLogPageComponent

        DriveLogPage {
            id: driveLogPage
        }
    }

    Component {
        id: addMalfunctionLogComponent

        AddMalfunctionLogPage {
            id: addMalfunctionLogPage
        }
    }

    Component {
        id: addRefuellingLogComponent

        AddRefuellingLogPage {
            id: addRefuellingLogPage
        }
    }

    function showCarTodo(carName) {
        mainWindow.pageStack.push(carTodoPageComponent, { "carName" : carName } );
    }

    Component {
        id: carTodoPageComponent

        TodoPage {
            id: todoPage
        }
    }

    Dialog {
        id: errorDialog

        property alias text: messageText.text

        anchors.centerIn: parent
        modal: true
        visible: false

        Text {
            id: messageText
            anchors.fill: parent
        }
    }

    LogTypeDialog {
        id: logTypeDialog

        anchors.centerIn: parent

        modal: true
        visible: false

        onAccepted: openAddLogDialog(typeCombo.currentValue)
    }

    function showError(error) {
        errorDialog.text = error;
        errorDialog.open();
    }

    function openAddLogDialog(typeCode) {
        switch (typeCode) {
        case "d":
            addDriveLogDialog.open()
            break;
        case "f":
            addRefuelMainLogDialog.open()
            break;
        case "e":
            addMalfunctionLogDialog.open()
            break;
        default:
            console.error("Invalid new log type code:", typeCode)
        }
    }

    function deleteCar(carName) {
        deleteCarPrompt.carName = carName;
        deleteCarPrompt.open();
    }

    Kirigami.OverlaySheet {
        id: deleteCarPrompt

        property string carName: ""

        header: Label {
            text: qsTr("Delete car")
        }

        contentItem: ColumnLayout {
            Label {
                text: qsTr("Do you really want to delete %1?").arg(deleteCarPrompt.carName)
                wrapMode: Text.WordWrap
                Layout.fillWidth: true
            }
            Button {
                text: qsTr("Delete")
                onClicked: {
                    Garage.deleteCar(deleteCarPrompt.carName);
                    deleteCarPrompt.close();
                }
            }
        }
    }
}

