#include "garage.h"

#include <algorithm>

#include <QDir>
#include <QSettings>
#include <QStandardPaths>

#include <QDebug>

using namespace std;

Garage::Garage(QObject *parent) : QObject(parent)
{
    QSettings settings;
    m_dirPath = settings.value("garage/dirPath").toString();
    if (m_dirPath.isEmpty())
        m_dirPath = QStandardPaths::writableLocation(QStandardPaths::AppDataLocation);

    QDir garageDir(m_dirPath.toString());
    if(!garageDir.exists()) {
        bool success = garageDir.mkpath(garageDir.path());
        if (!success)
            qWarning("Failed to create requested dir %s", qPrintable(garageDir.path()));
    }

    indexDirectory();
}

Car* Garage::getCar(const QString &name)
{
    Q_ASSERT(!name.isEmpty());

    auto car = find_if(m_cars.begin(), m_cars.end(),
        [name](auto& car) { return car->name() == name; });
    Q_ASSERT(car != m_cars.end());

    return *car;
}

bool Garage::createCar(const QString &name)
{
    qDebug("Create car called");

    // TODO: use find_if
    QStringList existingNames;
    transform(m_cars.cbegin(), m_cars.cend(), back_inserter(existingNames), [](auto& car) {
        return car->name();
    });
    if (existingNames.contains(name))
        return false;

    QDir garageDir(m_dirPath.toString());
    bool success = garageDir.mkdir(name);
    if (!success)
        return false;

    // TODO: create directory contents

    indexDirectory();

    return true;
}

bool Garage::deleteCar(const QString &name)
{
    qDebug("Delete car called");

    QDir garageDir(m_dirPath.toString());
    // TODO: remove dir contents
    bool success = garageDir.rmdir(name);
    if (!success)
        return false;

    indexDirectory();

    return true;
}

QUrl Garage::dirPath() const
{
    return m_dirPath;
}

QList<QObject*> Garage::cars() const
{
    QList<QObject*> carObjects;

    transform(m_cars.begin(), m_cars.end(), back_inserter(carObjects),
        [](auto carPointer) -> QObject*  { return reinterpret_cast<QObject*>(carPointer); });

    return carObjects;
}

void Garage::setDirPath(QUrl dirPath)
{
    if (m_dirPath == dirPath)
        return;

    qDebug("Garage dir set to: %s", qPrintable(dirPath.toString()));

    m_dirPath = dirPath;

    QDir garageDir(m_dirPath.toString());
    if(!garageDir.exists()) {
        bool success = garageDir.mkpath(garageDir.path());
        if (!success)
            qWarning("Failed to create requested dir %s", qPrintable(garageDir.path()));
    }

    emit dirPathChanged(m_dirPath);
}

void Garage::indexDirectory()
{
    QDir garageDir(m_dirPath.toString());
    auto carDirs = garageDir.entryInfoList(QDir::Dirs | QDir::NoDotAndDotDot);

    auto createCar = [&](QFileInfo fileInfo) -> Car* {
        auto car = new Car(this);
        car->setDirPath(fileInfo.filePath());
        return car;
    };

    m_cars.clear();
    transform(carDirs.cbegin(), carDirs.cend(), back_inserter(m_cars), createCar);

    emit carsChanged(cars());
}
