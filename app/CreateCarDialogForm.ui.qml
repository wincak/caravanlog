import QtQuick 2.0
import QtQuick.Controls 2.12

Dialog {
    id: dialog

    property alias carNameEdit: carNameEdit
    width: 320
    height: 240

    modal: true
    title: qsTr("Create car")
    standardButtons: Dialog.Ok | Dialog.Cancel

    Label {
        id: carNameLabel
        text: qsTr("Name")
        anchors.top: parent.top
        anchors.horizontalCenter: parent.horizontalCenter
    }

    TextField {
        id: carNameEdit
        width: 200
        height: 40
        anchors.top: carNameLabel.bottom
        anchors.horizontalCenter: parent.horizontalCenter
    }
}
