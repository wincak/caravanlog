#include "utils.h"

#ifdef Q_OS_ANDROID
#include <QtAndroidExtras>
#endif /* Q_OS_ANDROID */


///
/// \brief Utils::checkAndroidStoragePermissions
/// \return true if storage permissions granted by user.
/// false if denied.
///
Utils::Utils(QObject *parent)
    : QObject(parent)
{
}

bool Utils::checkAndroidStoragePermissions() {
#ifdef Q_OS_ANDROID
    const auto permissionsRequest = QStringList(
        { QString("android.permission.READ_EXTERNAL_STORAGE"),
            QString("android.permission.WRITE_EXTERNAL_STORAGE") });
    if (   (QtAndroid::checkPermission(permissionsRequest[0])
             == QtAndroid::PermissionResult::Denied)
        || (QtAndroid::checkPermission(permissionsRequest[1]))
             == QtAndroid::PermissionResult::Denied) {
        auto permissionResults
             = QtAndroid::requestPermissionsSync(permissionsRequest);
        if (   (permissionResults[permissionsRequest[0]]
                 == QtAndroid::PermissionResult::Denied)
            || (permissionResults[permissionsRequest[1]]
                == QtAndroid::PermissionResult::Denied))
            return (false);
    }
#endif /* Q_OS_ANDROID */
    return (true);
}

QString Utils::logTypeCodeToTypeName(const QChar &typeCode)
{
    QString typeName;

    if (typeCode == "d") {
        typeName = tr("Drive");
    } else if (typeCode == "f") {
        typeName = tr("Refuelling - main fuel");
    } else if (typeCode == "g") {
        typeName = tr("Refuelling - generator");
    } else if (typeCode == "p") {
        typeName = tr("Refuelling - propane");
    } else if (typeCode == "e") {
        typeName = tr("Malfunction");
    } else if (typeCode == "m") {
        typeName = tr("Maintenance");
    } else if (typeCode == "r") {
        typeName = tr("Repair");
    } else if (typeCode == "o") {
        typeName = tr("Other");
    }

    return typeName;
}
