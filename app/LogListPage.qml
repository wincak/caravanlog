import QtQuick 2.12
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.12

import org.kde.kirigami 2.13 as Kirigami

import name.wincak.caravanlog 1.0

Kirigami.Page {
    title: qsTr("Logs (%1)").arg(carName)

    property string carName: ""

    onCarNameChanged: {
        const car = Garage.getCar(carName);
        logList.model = car.logList;
    }

    ListView {
        id: logList
        anchors.fill: parent

        delegate: LogEntryDelegate {
            logDateText: date
            logTypeNameText: typeName
            logTypeCode: typeCode
            logDescriptionText: description
            mileageText: mileage
        }

        // There is no scroll bar by default
        ScrollBar.vertical: ScrollBar {
        }

        // Prevent view from drawing under surroundings
        clip: true
    }

    Label {
        id: defaultMessage
        text: qsTr("No logs found")
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top: parent.top
        anchors.topMargin: 100
        visible: logList.count <= 0
    }

    actions {
        contextualActions: [
            Kirigami.Action {
                text: qsTr("Add log entry")
                onTriggered: mainWindow.addNewLog(carName)
            }
        ]
    }
}
