#pragma once

#include "loglist.h"

#include <QObject>

class Car : public QObject {
    Q_OBJECT

    Q_PROPERTY(QUrl dirPath READ dirPath WRITE setDirPath NOTIFY dirPathChanged)

    Q_PROPERTY(QUrl logFilePath READ logFilePath NOTIFY logFilePathChanged)
    Q_PROPERTY(QUrl todoFilePath READ todoFilePath NOTIFY todoFilePathChanged)

    Q_PROPERTY(QString name READ name CONSTANT)

    // Localized strings
    Q_PROPERTY(QString mileageString READ mileageString NOTIFY mileageStringChanged)
    Q_PROPERTY(QString fuelUsedString READ fuelUsedString NOTIFY fuelUsedStringChanged)
    Q_PROPERTY(QString fuelCostString READ fuelCostString NOTIFY fuelCostStringChanged)
    Q_PROPERTY(QString avgConsumptionString READ avgConsumptionString NOTIFY avgConsumptionStringChanged)

    Q_PROPERTY(QObject* logList READ logList CONSTANT)

    Q_PROPERTY(QString error READ error WRITE setError NOTIFY errorChanged)

public:
    explicit Car(QObject* parent = nullptr);

    QUrl dirPath() const;

    QUrl logFilePath() const;
    QUrl todoFilePath() const;

    QString name() const;
    uint mileage() const;
    uint fuelUsed() const;
    uint fuelCost() const;

    QString mileageString() const;
    QString fuelUsedString() const;
    QString fuelCostString() const;

    QString avgConsumptionString() const;

    QObject* logList() const;

    Q_INVOKABLE bool appendDriveLog(const QDate& date, uint mileage, const QString& comment);
    Q_INVOKABLE bool appendMalfunctionLog(
        const QDate& date, uint mileage, const QString& todo, const QString& comment);
    Q_INVOKABLE bool appendRefuelMainLog(
        const QDate& date, uint mileage, float volume, float price, const QString& comment);

    QString error() const;

public slots:
    void setDirPath(QUrl dirPath);

signals:
    void dirPathChanged(QUrl dirPath);

    void logFilePathChanged(QUrl logFilePath);
    void todoFilePathChanged(QUrl todoFilePath);

    void mileageStringChanged(QString mileageString);
    void fuelUsedStringChanged(QString fuelUsedString);
    void fuelCostStringChanged(QString fuelCostString);

    void avgConsumptionStringChanged(QString avgConsumptionString);

    void errorChanged(QString error);

private:
    [[nodiscard]] bool readLog();
    [[nodiscard]] bool saveLog();

    void setLogFilePath(QUrl logFilePath);
    void setTodoFilePath(QUrl todoFilePath);

    void setMileage(uint mileage);
    void setFuelUsed(uint fuelUsed);
    void setFuelCost(uint fuelCost);

    void calculateAvgConsumption(uint fuelUsed, uint mileage);

    void setError(QString error);

private:
    QUrl m_dirPath;

    QUrl m_infoFilePath;
    QUrl m_logFilePath;
    QUrl m_todoFilePath;

    uint m_mileage;
    uint m_fuelUsed;
    uint m_fuelCost;

    QString m_mileageString;
    QString m_fuelUsedString;
    QString m_fuelCostString;

    QString m_avgConsumtionString;

    QList<AbstractLogMessage*> m_logMessages;
    QPointer<LogList> m_logList;

    QString m_error;
};
