import QtQuick 2.12
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.3

import Qt.labs.settings 1.1

import org.kde.kirigami 2.13 as Kirigami

import name.wincak.caravanlog 1.0

Kirigami.ScrollablePage {
    id: carsPage

    title: qsTr("Cars")

    Kirigami.CardsListView {
        id: carsView
        model: Garage.cars

        delegate: Kirigami.Card {
            contentItem: Label {
                text: modelData.name
            }

            actions: [
                Kirigami.Action {
                    text: qsTr("Details")
                    onTriggered: mainWindow.showCarDetails(modelData.name)
                },
                Kirigami.Action {
                    text: qsTr("Show log")
                    onTriggered: mainWindow.showCarLog(modelData.name)
                },
                Kirigami.Action {
                    text: qsTr("Add log")
                    onTriggered: mainWindow.addNewLog(modelData.name)
                },
                Kirigami.Action {
                    text: qsTr("TODO")
                    onTriggered: mainWindow.showCarTodo(modelData.name)
                },
                Kirigami.Action {
                    text: qsTr("Delete")
                    onTriggered: mainWindow.deleteCar(modelData.name)
                }
            ]
        }
    }

    actions.main: Kirigami.Action {
        text: qsTr("Create car")
        onTriggered: mainWindow.createCar()
    }

    Label {
        id: garageEmptyLabel
        anchors.centerIn: parent
        text: qsTr("Your garage is empty :(")
        visible: Garage.cars.length === 0
    }

}
