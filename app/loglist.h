#pragma once

#include <QAbstractListModel>

#include "LogMessage/abstractlogmessage.h"

#include "utils.h"

class LogList : public QAbstractListModel
{
    Q_OBJECT

public:
    enum LogRole {
        DateRole,
        TypeNameRole,
        TypeCodeRole,
        MileageRole,
        CommentRole,
        DescriptionRole,
    };

public:
    explicit LogList(QObject *parent = nullptr);

    // Header:
    QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;

    // Roles:
    QHash<int, QByteArray> roleNames() const override;

    // Basic functionality:
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;

    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

public slots:
    void setSourceData(QList<AbstractLogMessage*> sourceData);

private:
    void parseSourceData();

    struct DataLine {
        QString date;
        QString typeName;
        QString typeCode;
        QString mileage;
        QString comment;
        QString description;
    };

private:
    QVector<DataLine> m_parsedData;
};

