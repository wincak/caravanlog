#include <QApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include <QtPlugin>

#include "car.h"
#include "fileio.h"
#include "garage.h"
#include "loglist.h"
#include "utils.h"

#ifdef Q_OS_ANDROID
# include "../3rdparty/kirigami/src/kirigamiplugin.h"
Q_IMPORT_PLUGIN(KirigamiPlugin)
#endif

int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

    QApplication app(argc, argv);

    QCoreApplication::setApplicationName("CaravanLog");
    QCoreApplication::setOrganizationName("wincak");
    QCoreApplication::setOrganizationDomain("name.wincak");

    // Create garage
    auto garage = new Garage(&app);

    // Register C++ types
    qmlRegisterType<LogList>("name.wincak.caravanlog", 1, 0, "LogList");
    qmlRegisterType<Car>("name.wincak.caravanlog", 1, 0, "Car");
    qmlRegisterType<FileIo>("name.wincak.caravanlog", 1, 0, "FileIo");

    QQmlApplicationEngine engine;
#ifdef Q_OS_ANDROID
    KirigamiPlugin::getInstance().registerTypes(&engine);
#endif

    qmlRegisterSingletonInstance("name.wincak.caravanlog", 1, 0, "Garage", garage);

    Utils utils;
    engine.rootContext()->setContextProperty("utils", &utils);

    const QUrl url(QStringLiteral("qrc:/main.qml"));
    QObject::connect(&engine, &QQmlApplicationEngine::objectCreated,
        &app, [url](QObject *obj, const QUrl &objUrl) {
            if (!obj && url == objUrl)
                QCoreApplication::exit(-1);
        }, Qt::QueuedConnection);
    engine.load(url);

    return app.exec();
}
