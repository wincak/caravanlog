#include "car.h"
#include "fileio.h"

#include "LogMessage/drivelogmessage.h"
#include "LogMessage/malfunctionlogmessage.h"
#include "LogMessage/refuelmainlogmessage.h"

const QString logFileName = "log.json";
const QString todoFileName = "todo.txt";

// Monetary locale (prices)
const QString lcMonetary = QStringLiteral("LC_MONETARY");
// Measurement locale (kilometers/miles, liters/gallons)
const QString lcMeasurement = QStringLiteral("LC_MEASUREMENT");

Car::Car(QObject* parent)
    : QObject(parent)
{
    m_logList = new LogList(this);
}

QUrl Car::dirPath() const
{
    return m_dirPath;
}

QUrl Car::logFilePath() const
{
    return m_logFilePath;
}

QUrl Car::todoFilePath() const
{
    return m_todoFilePath;
}

QString Car::name() const
{
    return m_dirPath.fileName();
}

uint Car::mileage() const
{
    return m_mileage;
}

uint Car::fuelUsed() const
{
    return m_fuelUsed;
}

uint Car::fuelCost() const
{
    return m_fuelCost;
}

QString Car::mileageString() const
{
    return m_mileageString;
}

QString Car::fuelUsedString() const
{
    return m_fuelUsedString;
}

QString Car::fuelCostString() const
{
    return m_fuelCostString;
}

QString Car::avgConsumptionString() const
{
    return m_avgConsumtionString;
}

QObject *Car::logList() const
{
    return m_logList;
}

bool Car::appendDriveLog(const QDate &date, uint mileage, const QString &comment)
{
    auto msg = new DriveLogMessage(date, mileage, comment);
    if (!msg->isValid())
        return false;

    m_logMessages.append(msg);

    bool success = saveLog();
    if (!success)
        return false;

    // Log data is reloaded
    success = readLog();
    if (!success)
        return false;

    // Everything went OK, clearing error
    setError("");

    return true;
}

bool Car::appendMalfunctionLog(
    const QDate& date, uint mileage, const QString& todo, const QString& comment)
{
    auto msg = new MalfunctionLogMessage(date, mileage, comment);
    if (!msg->isValid())
        return false;

    m_logMessages.append(msg);

    bool success = saveLog();
    if (!success)
        return false;

    // Log data is reloaded
    success = readLog();
    if (!success)
        return false;

    FileIo fileIo;
    fileIo.setFilePath(m_todoFilePath);

    auto todoLine = QString("TODO: " + todo);
    success = fileIo.appendLine(todoLine);
    if (!success)
        return false;

    // Everything went OK, clearing error
    setError("");

    return true;
}

bool Car::appendRefuelMainLog(
    const QDate& date, uint mileage, float volume, float price, const QString& comment)
{
    auto msg = new RefuelMainLogMessage(date, mileage, volume, price, comment);
    if (!msg->isValid())
        return false;

    m_logMessages.append(msg);

    bool success = saveLog();
    if (!success)
        return false;

    // Log data is reloaded
    success = readLog();
    if (!success)
        return false;

    // Everything went OK, clearing error
    setError("");

    return true;
}

QString Car::error() const
{
    return m_error;
}

void Car::setDirPath(QUrl dirPath)
{
    if (m_dirPath == dirPath)
        return;

    QString localPath = dirPath.toString();
    QDir carDir(dirPath.toString());
    if (!carDir.exists()) {
        qWarning("Car dir %s does not exist", qPrintable(localPath));
        return;
    }

    m_dirPath = dirPath;

    // Process all car files (existing and non-existent)
    setLogFilePath(carDir.filePath(logFileName));
    setTodoFilePath(carDir.filePath(todoFileName));

    emit dirPathChanged(m_dirPath);
}

bool Car::readLog()
{
    m_logMessages.clear();

    if (!m_logFilePath.isValid()) {
        setError(tr("File source is not set."));
        return false;
    }

    QString localPath = m_logFilePath.toString();

    QFile file(localPath);
    // If the file does not exist, there is nothing to read
    if (!file.exists())
        return true;

    bool success = file.open(QFile::ReadOnly);
    if (!success) {
        setError(tr("Failed to open file %1 for reading.") .arg(m_logFilePath.toString()));
        return false;
    }

    QJsonParseError parseError;
    auto doc = QJsonDocument::fromJson(file.readAll(), &parseError);
    if (parseError.error != QJsonParseError::NoError) {
        setError(parseError.errorString());
        return false;
    }

    QVariantMap map = doc.toVariant().toMap();
    if (map.isEmpty()) {
        setError(tr("Invalid log file format"));
        return false;
    }

    // TODO: process car info

    // Collect data
    uint newMileage = 0;
    uint newFuelUsed = 0;
    uint newFuelCost = 0;
    auto logs = map.value("logs").toList();
    for (auto log : logs) {
        Q_ASSERT(log.type() == QVariant::Map);

        QString typeCode = AbstractLogMessage::typeCode(log);
        if (typeCode == DriveLogMessage::typeCode()) {
            auto message = new DriveLogMessage(DriveLogMessage::fromVariant(log));
            if (!message->isValid())
                continue;

            m_logMessages.append(message);
            if (message->mileage() > newMileage)
                newMileage = message->mileage();
        } else if (typeCode == MalfunctionLogMessage::typeCode()) {
            auto message = new MalfunctionLogMessage(MalfunctionLogMessage::fromVariant(log));
            if (!message->isValid())
                continue;

            m_logMessages.append(message);
            if (message->mileage() > newMileage)
                newMileage = message->mileage();
        } else if (typeCode == RefuelMainLogMessage::typeCode()) {
            auto message = new RefuelMainLogMessage(RefuelMainLogMessage::fromVariant(log));
            if (!message->isValid())
                continue;

            m_logMessages.append(message);
            if (message->mileage() > newMileage)
                newMileage = message->mileage();

            newFuelUsed += message->volume();
            newFuelCost += message->price();
        } else {
            qWarning("Unknown log message type code %s", qPrintable(typeCode));
        }
    }

    // Update data
    setMileage(newMileage);
    setFuelUsed(newFuelUsed);
    setFuelCost(newFuelCost);

    // Calculate statistics
    calculateAvgConsumption(newFuelUsed, newMileage);

    m_logList->setSourceData(m_logMessages);

    return true;
}

bool Car::saveLog()
{
    if (!m_logFilePath.isValid()) {
        setError(tr("Log file path is not set."));
        return false;
    }

    // FIXME: QFile cannot handle the file:// URI that is standard on desktop Qt. On the other hand
    // it requires the content:// prefix used on Android
    QString localPath;
    localPath = m_logFilePath.toString();

    QFile file(localPath);
    bool success = file.open(QIODevice::WriteOnly);
    if (!success) {
        setError(tr("Failed to open file %1 for appending.").arg(m_logFilePath.toString()));
        return false;
    }

    QVariantList messages;
    std::transform(m_logMessages.cbegin(), m_logMessages.cend(), std::back_inserter(messages),
        [](AbstractLogMessage* message) { return message->toVariant(); });

    QVariantMap map;
    map.insert("logs", messages);

    auto doc = QJsonDocument::fromVariant(map);

    QTextStream textStream(&file);
    textStream << doc.toJson() << Qt::endl;

    return true;
}

void Car::setLogFilePath(QUrl logFilePath)
{
    if (m_logFilePath == logFilePath)
        return;

    m_logFilePath = logFilePath;
    emit logFilePathChanged(m_logFilePath);

    bool success = readLog();
    if (!success)
        return;

    // Everything went OK, clearing error
    setError("");

    return;
}

void Car::setTodoFilePath(QUrl todoFilePath)
{
    if (m_todoFilePath == todoFilePath)
        return;

    m_todoFilePath = todoFilePath;
    emit todoFilePathChanged(m_todoFilePath);
}

void Car::setMileage(uint mileage)
{
    if (m_mileage == mileage)
        return;

    m_mileage = mileage;

    QString unit;
    auto measurementLocale = QLocale(qgetenv(lcMeasurement.toLatin1()));
    switch (measurementLocale.measurementSystem()) {
    case QLocale::MetricSystem:
        unit = tr("km");
        break;
    case QLocale::ImperialUSSystem:
    case QLocale::ImperialUKSystem:
        unit = tr("mi");
    }

    m_mileageString = QString("%1 %2").arg(QLocale::system().toString(m_mileage)).arg(unit);

    emit mileageStringChanged(m_mileageString);
}

void Car::setFuelUsed(uint fuelUsed)
{
    if (m_fuelUsed == fuelUsed)
        return;

    m_fuelUsed = fuelUsed;

    QString unit;
    auto measurementLocale = QLocale(qgetenv(lcMeasurement.toLatin1()));
    switch (measurementLocale.measurementSystem()) {
    case QLocale::MetricSystem:
        unit = tr("l");
        break;
    case QLocale::ImperialUSSystem:
    case QLocale::ImperialUKSystem:
        unit = tr("gal");
    }

    m_fuelUsedString = QString("%1 %2").arg(QLocale::system().toString(m_fuelUsed)).arg(unit);

    emit fuelUsedStringChanged(m_fuelUsedString);
}

void Car::setFuelCost(uint fuelCost)
{
    if (m_fuelCost == fuelCost)
        return;

    m_fuelCost = fuelCost;
    m_fuelCostString = QLocale(qgetenv(lcMonetary.toLatin1())).toCurrencyString(m_fuelCost);
    emit fuelCostStringChanged(m_fuelCostString);
}

void Car::calculateAvgConsumption(uint fuelUsed, uint mileage)
{
    float avgConsumption;
    QString unit;

    auto measurementLocale = QLocale(qgetenv(lcMeasurement.toLatin1()));
    switch (measurementLocale.measurementSystem()) {
    case QLocale::MetricSystem:
        // liters per 100km
        avgConsumption = (static_cast<float>(fuelUsed) / static_cast<float>(mileage)) * 100.0;
        unit = tr("l/100km");
    break;
    case QLocale::ImperialUSSystem:
    case QLocale::ImperialUKSystem:
        // miles per gallon
        avgConsumption = (static_cast<float>(mileage) / static_cast<float>(fuelUsed));
        unit = tr("mpg");
    }

    m_avgConsumtionString
        = QString("%1 %2").arg(QLocale::system().toString(avgConsumption, 'f', 2)).arg(unit);

    emit avgConsumptionStringChanged(m_avgConsumtionString);
}

void Car::setError(QString error)
{
    if (m_error == error)
        return;

    m_error = error;
    emit errorChanged(m_error);
}
