import QtQuick 2.4
import QtQuick.Dialogs 1.3 as Dlgs

import name.wincak.caravanlog 1.0

SettingsPageForm {
    chooseDataDirectoryButton.onClicked: dataDirSelector.open()

    dataDirectoryPath.text: String(Garage.dirPath)

    Dlgs.FileDialog {
        id: dataDirSelector
        visible: false
        selectFolder: true
        selectExisting: true
        selectMultiple: false

        onAccepted: Garage.dirPath = fileUrl
    }
}
