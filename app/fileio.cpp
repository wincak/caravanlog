#include "fileio.h"

#include <QFile>
#include <QSaveFile>
#include <QTimer>

FileIo::FileIo(QObject *parent)
    : QObject(parent)
{
}

QUrl FileIo::filePath() const
{
    return m_filePath;
}

bool FileIo::appendLine(const QString &line)
{
    if (!m_filePath.isValid()) {
        setError(tr("File source is not set."));
        return false;
    }

    QString path = m_filePath.toString(QUrl::RemoveScheme);
    QSaveFile file(path);
    bool success = file.open(QIODevice::WriteOnly | QIODevice::Text);
    if (!success) {
        setError(tr("Failed to open file for writing."));
        return false;
    }

    // QSaveFile does not support QIODevice::Append mode
    QByteArray data(m_text.toUtf8());
    if (!data.endsWith('\n'))
        data.append('\n');

    data.append(line.toUtf8());
    if (!data.endsWith('\n'))
        data.append('\n');

    auto written = file.write(data);
    if (written <= 0) {
        setError(tr("Failed to write data."));
        return false;
    }
    success = file.commit();
    if (!success) {
        setError(tr("Failed to commit data."));
        return false;
    }

    QString errorText;
    QString newText = readContents(&errorText);

    setError(errorText);

    return errorText.isEmpty();
}

QString FileIo::text() const
{
    return m_text;
}

QString FileIo::error() const
{
    return m_error;
}

void FileIo::setFilePath(const QUrl &filePath)
{
    if (m_filePath == filePath)
        return;

    m_filePath = filePath;
    emit filePathChanged(m_filePath);

    QString errorText;
    QString newText = readContents(&errorText);

    setError(errorText);

    m_text = newText;
    emit textChanged(m_text);
}

void FileIo::setText(const QString &text)
{
    if (m_text == text)
        return;

    m_text.clear();

    if (!m_filePath.isValid()) {
        setError(tr("Invalid TODO file path"));
        return;
    }

    QString pathString = m_filePath.toString(QUrl::RemoveScheme);
    QSaveFile file(pathString);
    bool success = file.open(QIODevice::WriteOnly | QIODevice::Text);
    if (!success) {
        setError(tr("Failed to open file for writing"));
        return;
    }

    auto written = file.write(text.toUtf8());
    if (written <= 0) {
        setError(tr("Failed to write data."));
        return;
    }
    success = file.commit();
    if (!success) {
        setError(tr("Failed to commit data."));
        return;
    }

    // Clear error
    setError("");

    m_text = text;
    emit textChanged(m_text);
}

void FileIo::setError(const QString &error)
{
    if (m_error == error)
        return;

    m_error = error;
    emit errorChanged(m_error);
}

QString FileIo::readContents(QString *error)
{
    QString newText;

    if (!m_filePath.isValid()) {
        if(error)
            *error = tr("Invalid file path");

        return newText;
    }

    QString path = m_filePath.toString(QUrl::RemoveScheme);
    QFile file(path);
    // If the file does not exist, there is nothing to read
    if (file.exists()) {
        bool success = file.open(QIODevice::ReadOnly | QIODevice::Text);
        if (!success) {
            if (error)
                *error = tr("Failed to open file for reading.");

            return newText;
        }

        newText = file.readAll();
    }

    return newText;
}
