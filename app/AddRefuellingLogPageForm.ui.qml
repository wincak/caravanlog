import QtQuick 2.4
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.12

import org.kde.kirigami 2.13 as Kirigami

Kirigami.Page {
    id: dialog

    title: qsTr("Add refuelling log")

    property alias mileage: mileage
    property alias selectedDateLabel: selectedDateLabel
    property alias dateSetButton: dateSetButton
    property alias volume: volume
    property alias price: price
    property alias comment: comment

    ColumnLayout {
        id: content
        anchors.fill: parent

        Label {
            id: dateLabel
            text: qsTr("Date")
            Layout.fillWidth: true
        }
        Pane {
            // TODO: polish and make into a separate GUI component
            id: datePickerRow
            Layout.preferredHeight: 40
            Layout.fillWidth: true
            padding: 0

            //border.color: "gray"
            //border.width: 1
            RowLayout {
                anchors.fill: parent

                Label {
                    id: selectedDateLabel
                    text: qsTr("Today")
                    Layout.fillWidth: true
                    leftPadding: 5
                }
                Button {
                    id: dateSetButton
                    text: qsTr("Set")
                }
            }
        }

        Label {
            id: mileageLabel
            text: qsTr("Mileage")
            Layout.fillWidth: true
        }
        SpinBox {
            id: mileage
            wheelEnabled: true
            editable: true
            to: 9999999
            Layout.fillWidth: true
        }

        Label {
            id: volumeLabel
            text: qsTr("Volume")
            Layout.fillWidth: true
        }
        SpinBox {
            id: volume
            wheelEnabled: true
            editable: true
            to: 9999999
            Layout.fillWidth: true

            stepSize: 100
            property int decimals: 2
            property real realValue: value / 100
        }

        Label {
            id: priceLabel
            text: qsTr("Price")
            Layout.fillWidth: true
        }
        SpinBox {
            id: price
            wheelEnabled: true
            editable: true
            to: 9999999
            Layout.fillWidth: true

            stepSize: 100
            property int decimals: 2
            property real realValue: value / 100
        }

        Label {
            id: commentLabel
            text: qsTr("Comment")
            Layout.fillWidth: true
        }
        TextArea {
            id: comment
            placeholderText: qsTr("comment")
            Layout.fillWidth: true
            Layout.fillHeight: true
        }
    }
}
