#pragma once

#include <QtCore>

class Utils : public QObject
{
    Q_OBJECT

public:
    Utils(QObject* parent = nullptr);
    Q_INVOKABLE bool checkAndroidStoragePermissions();

    static QString logTypeCodeToTypeName(const QChar& typeCode);
};

