import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12

import org.kde.kirigami 2.13 as Kirigami

Kirigami.SwipeListItem {
    id: element

    property alias itemIcon: itemIcon
    property alias dateTimeLabel: dateTimeLabel
    property alias logTypeLabel: logTypeLabel
    property alias descriptionLabel: descriptionLabel
    property alias mileageLabel: mileageLabel

    RowLayout {
        Text {
            id: itemIcon

            Layout.preferredWidth: 40
            Layout.preferredHeight: 40

            text: "?"

            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter

            font.pointSize: 18
        }

        ColumnLayout {
            Layout.minimumWidth: 100
            Layout.fillWidth: true

            RowLayout {
                Label {
                    id: logTypeLabel

                    Layout.fillWidth: true
                    text: "Drive"
                    font.bold: true
                }
                Label {
                    id: dateTimeLabel

                    Layout.alignment: Qt.AlignRight
                    text: "1.1.1970, 13:37"
                }
            }
            RowLayout {
                Label {
                    id: descriptionLabel

                    Layout.fillWidth: true
                    clip: true
                    elide: Text.ElideRight
                    text: "Description"
                }

                Label {
                    id: mileageLabel

                    Layout.minimumWidth: 80
                    Layout.alignment: Qt.AlignRight
                    horizontalAlignment: Text.AlignRight
                    text: "123456 km"
                }
            }
        }
    }

}
