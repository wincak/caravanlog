import QtQuick 2.12

import QtQuick.Dialogs 1.2 as Dlgs

import name.wincak.caravanlog 1.0

SubmitPageForm {
    id: page

    signal newLogRequested

    fileUrlView.text: car.logFilePath

    chooseFileButton.onClicked: fileSelector.open()
    addLogButton.onClicked: newLogRequested()
    testFsAccessButton.onClicked: checkFsAccess()

    Dlgs.FileDialog {
        id: fileSelector
        visible: false
        selectExisting: false
        onAccepted: car.logFilePath = fileSelector.fileUrl
    }

    function checkFsAccess() {
        const access = utils.checkAndroidStoragePermissions();

        if (access) {
            testFsAccessButton.text = qsTr("FS access OK");
        } else {
            testFsAccessButton.text = qsTr("No FS access")
        }
    }
}
