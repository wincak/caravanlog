TEMPLATE = subdirs
SUBDIRS = app

android: {
    QT += androidextras
#    include(3rdparty/kirigami/kirigami.pri)
    # Build fails because of undefined qt_static_plugin_KirigamiPlugin
    # caused by Q_IMPORT_PLUGIN(KirigamiPlugin) in main.cpp
    # These attempts don't work
#    LIBS += -L3rdparty/kirigami -lorg/kde/kirigami.2/kirigamiplugin
#    LIBS += -L3rdparty/kirigami -lkirigamiplugin
    LIBS += \
        -L3rdparty/kirigami \
        -lkirigamiplugin

#    include(3rdparty/kirigami/kirigami.pro)
#    TEMPLATE = subdirs
    SUBDIRS += 3rdparty/kirigami
    app.depends = 3rdparty/kirigami
}

CONFIG += c++17

# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Refer to the documentation for the
# deprecated API to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

TRANSLATIONS += \
    CaravanLog_cs_CZ.ts

# Additional import path used to resolve QML modules just for Qt Quick Designer
QML_DESIGNER_IMPORT_PATH =

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

ANDROID_ABIS = armeabi-v7a
